﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using Android.App;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using Java.Interop;

namespace AppV
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.activity_main);

            Android.Support.V7.Widget.Toolbar toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);

            //FloatingActionButton fab = FindViewById<FloatingActionButton>(Resource.Id.fab);
            //fab.Click += FabOnClick;
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.menu_main, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            int id = item.ItemId;
            if (id == Resource.Id.action_settings)
            {
                return true;
            }

            return base.OnOptionsItemSelected(item);
        }

        private void FabOnClick(object sender, EventArgs eventArgs)
        {
            View view = (View) sender;
            Snackbar.Make(view, "Replace with your own action", Snackbar.LengthLong)
                .SetAction("Action", (Android.Views.View.IOnClickListener)null).Show();
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        //public void onClickSendPost(object sender, EventArgs args)
        //{
        //    Toast.MakeText(ApplicationContext, "", ToastLength.Long).Show();

        //    void OnButtonClicked(object sender, EventArgs e)
        //    {
        //        (sender as Button).Text = "Click me again!";
        //    }
        //}

        private string res_json = "";

        internal static string UnJavascriptEscape(string s)
        {
            return System.Text.RegularExpressions.Regex.Unescape(s);
        }

        private void Run()
        {

            
            string pathCer = "/mnt/sdcard/Download/" + "VTB2024.pfx";
            string passwCer = "";

            string json_NSPK_CreateAndGetQRcode = "{" +
                "\t\"agentId\": \"A00000000010\"," +
                "\t\"memberId\": \"100000000005\"," +
                "\t\"legalId\": \"LA0000006521\"," +
                "\t\"account\": \"40702810800180000669\"," +
                "\t\"merchantId\": \"MA0000083304\"," +
                "\t\"templateVersion\": \"01\"," +
                "\t\"qrcType\": \"02\"," +
                "\t\"amount\": \"1000\"," +
                "\t\"currency\": \"RUB\"," +
                "\t\"paymentPurpose\": \"string\"" +
                "}";

            string url_NSPK_CreateAndGetQRcode = "https://extcrmti.vtb24.ru:6443/v01/BankGateway/In/NSPK_CreateAndGetQRcode";

            //Toast.MakeText(this, "Hello from " + v.Id, ToastLength.Long).Show();

            res_json = Com.Refresh.Pos.Resr_vtb24.Resr_vtb24.Vtb24_request(url_NSPK_CreateAndGetQRcode, json_NSPK_CreateAndGetQRcode, pathCer, passwCer);

            
            //Console.WriteLine("res_json: " + res_json);
        }

        [Java.Interop.Export("button_OnClick")]
        public void button_OnClick(View v)
        {
            Thread th = new Thread(Run);
            th.Start();
            th.Join();

            Com.Refresh.Pos.Resr_vtb24.Resr_vtb24.MessageBox(ApplicationContext, "java Hello: " + UnJavascriptEscape(res_json));
        }
    }
}
