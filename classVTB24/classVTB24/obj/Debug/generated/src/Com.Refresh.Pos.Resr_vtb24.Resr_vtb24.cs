using System;
using System.Collections.Generic;
using Android.Runtime;
using Java.Interop;

namespace Com.Refresh.Pos.Resr_vtb24 {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.refresh.pos.resr_vtb24']/class[@name='resr_vtb24']"
	[global::Android.Runtime.Register ("com/refresh/pos/resr_vtb24/resr_vtb24", DoNotGenerateAcw=true)]
	public partial class Resr_vtb24 : global::Java.Lang.Object {
		static readonly JniPeerMembers _members = new XAPeerMembers ("com/refresh/pos/resr_vtb24/resr_vtb24", typeof (Resr_vtb24));

		internal static IntPtr class_ref {
			get { return _members.JniPeerType.PeerReference.Handle; }
		}

		[global::System.Diagnostics.DebuggerBrowsable (global::System.Diagnostics.DebuggerBrowsableState.Never)]
		[global::System.ComponentModel.EditorBrowsable (global::System.ComponentModel.EditorBrowsableState.Never)]
		public override global::Java.Interop.JniPeerMembers JniPeerMembers {
			get { return _members; }
		}

		[global::System.Diagnostics.DebuggerBrowsable (global::System.Diagnostics.DebuggerBrowsableState.Never)]
		[global::System.ComponentModel.EditorBrowsable (global::System.ComponentModel.EditorBrowsableState.Never)]
		protected override IntPtr ThresholdClass {
			get { return _members.JniPeerType.PeerReference.Handle; }
		}

		[global::System.Diagnostics.DebuggerBrowsable (global::System.Diagnostics.DebuggerBrowsableState.Never)]
		[global::System.ComponentModel.EditorBrowsable (global::System.ComponentModel.EditorBrowsableState.Never)]
		protected override global::System.Type ThresholdType {
			get { return _members.ManagedPeerType; }
		}

		protected Resr_vtb24 (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer)
		{
		}

		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.refresh.pos.resr_vtb24']/class[@name='resr_vtb24']/constructor[@name='resr_vtb24' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe Resr_vtb24 () : base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			const string __id = "()V";

			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				var __r = _members.InstanceMethods.StartCreateInstance (__id, ((object) this).GetType (), null);
				SetHandle (__r.Handle, JniHandleOwnership.TransferLocalRef);
				_members.InstanceMethods.FinishCreateInstance (__id, this, null);
			} finally {
			}
		}

		// Metadata.xml XPath method reference: path="/api/package[@name='com.refresh.pos.resr_vtb24']/class[@name='resr_vtb24']/method[@name='messageBox' and count(parameter)=2 and parameter[1][@type='android.content.Context'] and parameter[2][@type='java.lang.String']]"
		[Register ("messageBox", "(Landroid/content/Context;Ljava/lang/String;)V", "")]
		public static unsafe void MessageBox (global::Android.Content.Context c, string message)
		{
			const string __id = "messageBox.(Landroid/content/Context;Ljava/lang/String;)V";
			IntPtr native_message = JNIEnv.NewString (message);
			try {
				JniArgumentValue* __args = stackalloc JniArgumentValue [2];
				__args [0] = new JniArgumentValue ((c == null) ? IntPtr.Zero : ((global::Java.Lang.Object) c).Handle);
				__args [1] = new JniArgumentValue (native_message);
				_members.StaticMethods.InvokeVoidMethod (__id, __args);
			} finally {
				JNIEnv.DeleteLocalRef (native_message);
				global::System.GC.KeepAlive (c);
			}
		}

		// Metadata.xml XPath method reference: path="/api/package[@name='com.refresh.pos.resr_vtb24']/class[@name='resr_vtb24']/method[@name='vtb24_request' and count(parameter)=4 and parameter[1][@type='java.lang.String'] and parameter[2][@type='java.lang.String'] and parameter[3][@type='java.lang.String'] and parameter[4][@type='java.lang.String']]"
		[Register ("vtb24_request", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;", "")]
		public static unsafe string Vtb24_request (string url_addr, string jsonBody, string pathCer, string passw)
		{
			const string __id = "vtb24_request.(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;";
			IntPtr native_url_addr = JNIEnv.NewString (url_addr);
			IntPtr native_jsonBody = JNIEnv.NewString (jsonBody);
			IntPtr native_pathCer = JNIEnv.NewString (pathCer);
			IntPtr native_passw = JNIEnv.NewString (passw);
			try {
				JniArgumentValue* __args = stackalloc JniArgumentValue [4];
				__args [0] = new JniArgumentValue (native_url_addr);
				__args [1] = new JniArgumentValue (native_jsonBody);
				__args [2] = new JniArgumentValue (native_pathCer);
				__args [3] = new JniArgumentValue (native_passw);
				var __rm = _members.StaticMethods.InvokeObjectMethod (__id, __args);
				return JNIEnv.GetString (__rm.Handle, JniHandleOwnership.TransferLocalRef);
			} finally {
				JNIEnv.DeleteLocalRef (native_url_addr);
				JNIEnv.DeleteLocalRef (native_jsonBody);
				JNIEnv.DeleteLocalRef (native_pathCer);
				JNIEnv.DeleteLocalRef (native_passw);
			}
		}

	}
}
